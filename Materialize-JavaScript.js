 <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.collapsible');
      var instances = M.Collapsible.init(elems);
    });

    function showAboutContent() {
      hideAllContent();
      var aboutContent = document.querySelector('.about-content');
      aboutContent.style.display = 'block';
    }
    
    function showMainContent() {
      hideAllContent();
      var mainContent = document.querySelector('.main-content');
      mainContent.style.display = 'block';
    }

    function showChangelogContent() {
      hideAllContent();
      var changelogContent = document.querySelector('.changelog-content');
      changelogContent.style.display = 'block';
    }

    function showInstallsContent() {
      hideAllContent();
      var installsContent = document.querySelector('.installs-content');
      installsContent.style.display = 'block';
    }

    function hideAllContent() {
      var allContent = document.querySelectorAll('.main-content, .about-content, .changelog-content, .installs-content');
      allContent.forEach(function(element) {
        element.style.display = 'none';
      });
    }
  </script>